\documentclass{article}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}
\usepackage{float}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage[margin=2cm]{geometry}
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[LE,LO]{
      \hspace{-0.1cm} \begin{tabular}{c} \\[-1ex] \includegraphics[width=30mm]{Logo/TsetSE_logo.pdf}
           \\{\Large TsetSE} \end{tabular}
}
\fancyfoot[LE,RO]{TsetSE manual, page \thepage}
\setlength{\headheight}{3cm}
\setlength{\textheight}{21cm}

\renewcommand{\familydefault}{\sfdefault}
\title{TsetSE, the Tset suite that lets you See Everything\\
User manual}
\author{Bas van 't Hof, for Leiden university}
\date{Dec 14, 2020}

\usepackage{xcolor}
\definecolor{darkgreen}{rgb}{0.0, 0.4, 0.26}
\definecolor{darkred}{rgb}{0.6, 0.0, 0.0}
\usepackage{listings}
\lstset{ 
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  commentstyle=\color{darkgreen},    % comment style
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
}

\begin{document}
\newcommand{\cmConst}[1]{{\color{darkred}#1}}
\newcommand{\cmCmd}[1]{{\color{blue}#1}}
\maketitle
\tableofcontents 
\clearpage
\section{Testing computational code}
When testing computational code, assessing the correctness of results requires the comparison 
to presumably correct answers, either in the form of stored reference answers, theoretical results, or 
results from a 'stable' version of the software.

For such comparisons, admissable differences (tolerances) are very important. It has to be possible to specify
very precisely what is considered 'close enough' to the correct answer.

When non-negligable differences are detected, it is often necessary to visualize the results graphically, to see 
what the differences are.

The TsetSE test suite aims to provide some tools to help do such testing and assist in the visualization of the
tests. It is intended to be used in combination with existing testing platforms, such as {\tt ctest}, {\tt pytest},
{\tt doctest} or {\tt unittest}.

\section{A simple example}
Your delivery of TsetSE comes with a few sample tests. Let us look at the first test, called {\tt poly3}.
It starts with some imports:
\begin{lstlisting}[language=python,basicstyle=\ttfamily,columns=fullflexible]
import numpy as np
from TsetSE.GraphicsComparison import GraphicsComparison
\end{lstlisting}
Next, it has the function {\tt run}, which does some calculations and calls {\tt GraphicsComparison}: 
\begin{lstlisting}[language=python,basicstyle=\ttfamily,columns=fullflexible]
def run():
    x = np.arange(0,2,0.02)
    y = x * (x-0.5) * (x+0.3) + 0.2
    plotdata = { 'x': x, 'y': y, 'plotfun': plotfun}
    GraphicsComparison('poly3',plotdata, tol=1e-7, d=2)
\end{lstlisting}
Note that {\tt plotfun}, which is a function pointer, is passed to {\tt GraphicsComparison}. 
The relative tolerance {\tt tol} is optional and has the default value {\tt 1e-8}. 
It will be possible in future versions to specify tolerances for {\tt x} and {\tt y} separately.

The plot function produces the plots, and is given by
\begin{lstlisting}[language=python,basicstyle=\ttfamily,columns=fullflexible]
def plotfun(plotdata):
    """
    Plot function of test poly3
    ===========================
    This file simply plots a function y(x).
    The title is 'polynomial of order 3' (regardless of the inputs).
    """
    import matplotlib.pyplot as plt
    x, y = plotdata['x'], plotdata['y']
    plt.plot(x,y)
    plt.suptitle('a polynomial of order 3')
    plt.xlabel('x axis')
    plt.ylabel('y axis')
\end{lstlisting}

\section{Running all the test (text only)}
TsetSE comes with a program {\tt TsetSE\_run.py}, which is used to start the test:
\begin{lstlisting}[language=bash,basicstyle=\ttfamily,columns=fullflexible]
TsetSE_run.py --testfile tests/testfile.json
\end{lstlisting}
The script requires a file {\tt testfile.json}, which contains the names of the available tests:
\begin{lstlisting}[language=python,basicstyle=\ttfamily,columns=fullflexible] 
[ "poly3", "intentional_failure", "orangina" ]
\end{lstlisting}
The program creates the output files {\tt ntentional\_failure.pickle}, {\tt orangina.pickle} and 
{\tt poly3.pickle} in directory {\tt tests/TsetSEdat}.
It also prints the following lines of text:
\begin{lstlisting}[basicstyle=\ttfamily,columns=fullflexible]  
   ============================================================
     poly3                failed                in 0.013 seconds
     intentional_failure  failed                in 0.002 seconds
     orangina             failed                in 0.005 seconds

     0 tests passed successfully,  3  tests failed
   ============================================================
\end{lstlisting}
This is disappointing: all tests failed! The tests failed because the
reference output files are not yet available. They can be created by
copying the output files to the reference directory:
\begin{lstlisting}[language=bash,basicstyle=\ttfamily,columns=fullflexible]  
cp tests/TsetSEdat/*.pickle tests/TsetSEdat/references/
\end{lstlisting}
When run again, the program now outputs the following, more successful lines of text:
\begin{lstlisting}[basicstyle=\ttfamily,columns=fullflexible]  
   ============================================================
     poly3                finished successfully in 0.016 seconds
     intentional_failure  finished successfully in 0.007 seconds
     orangina             finished successfully in 0.004 seconds

     3 tests passed successfully,  0  tests failed
   ============================================================
\end{lstlisting}


\section{Viewing test results}
The script {\tt TsetSE\_run.py} creates output files. These files contain the plot-function in the form 
of a text string of the source code.  This plot-function can be run using the script {\tt TsetSE\_show.py}:
\begin{lstlisting}[language=bash,basicstyle=\ttfamily,columns=fullflexible]
TsetSE_show.py tests/TsetSEdat/orangina
\end{lstlisting}
\begin{figure}[H]
\begin{center}
\includegraphics[width=15cm]{figs/orangina.pdf}
\end{center}
\caption{\em Output of running {\tt TsetSE\_show.py tests/TsetSEdat/orangina}: the doc-string and the output of the plot-function}
\label{Fig: orangina}
\end{figure}
Figure \ref{Fig: orangina} shows the output of running {\tt TsetSE\_show}: the doc-string of the plot function is printed, 
and the plot is shown. The doc-string functions as a caption for the plot: what does the plot show: which test did the calculation? 

Since the plot function has been saved in the output file, the test-scripts are unnecessary for viewing the test results.
The output files contain all the information needed to view them.

\section{A safety warning}
When visualizing TsetSE output files, the string {\tt plotfun}, that contains the source code of the plot function, 
is passed to the function {\tt exec}. 

{\bf If harmful code is written in a TsetSE output file, it will be executed!}

Visualizing output files that you haven't created yourself is therefore dangerous!

The examples delivered with the TsetSE are no exception. This is why no output files are part of the 
TsetSE delivery.

\section{Comparing test results to references}
Especially in cases when TsetSE reports differences between outputs and references, 
it is useful to graphically compare them. This is done with {\tt TsetSE\_compare.py}:
\begin{lstlisting}[language=bash,basicstyle=\ttfamily,columns=fullflexible]
TsetSE_compare.py tests/TsetSEdat/intentional_failure --plot-on-error --linelen-doc 20
\end{lstlisting}
\begin{figure}[H]
\begin{center}
\includegraphics[width=16cm]{figs/intentional_failure.pdf}
\end{center}
\caption{\em Output of running {\tt TsetSE\_compare.py tests/TsetSEdat/intentional\_failure --plot-on-error}:
the doc-string, a report of the differences found, and the output of 
the plot-function for current results and reference results.}
\label{Fig: intentional failure}
\end{figure}

The results are shown in Figure \ref{Fig: intentional failure}. We recognize a lot from {\tt TsetSE\_show},
but this time there are two plots: one for the current results, and one for the references.
When zooming in or panning one plot, the other plot will move in the same way.

\section{Test platforms}
\subsection{Using TsetSE as its own platform}
TsetSE can be used as the test platform: it allows running all tests, or a selection of tests, for instance only re-running the 
tests that fail, or a single test. The script {\tt TsetSE\_run.pl} also contains the functionality of {\tt TsetSE\_show.py} and
{\tt TsetSE\_compare.py}, so it allows graphical comparison of outputs to references and it allows you to update the references.

When all tests finish succesfully, the program exits with a zero return code, if any of the tests fail, the return code is nonzero.
So, really, you could use TsetSE as your test suite, and in your Continuous Integration environment, and use no other test suite.

\subsection{Using TsetSE with Ctest}
\href{https://gitlab.kitware.com/cmake/community/-/wikis/doc/ctest/Testing-With-CTest}{CTest}
is a testing tool distributed as a part of CMake.  TsetSE tests can be added to Ctest using the following {\tt CMakeLists.txt}:
\\[1ex]
{\tt
\cmCmd{cmake\_minimum\_required}(\cmConst{VERSION} 3.18)\\
\cmCmd{project}(TsetSE)\\
\cmCmd{enable\_testing}()
\\[1ex]
\cmCmd{set}(testfile "\cmConst{\$\{CMAKE\_CURRENT\_SOURCE\_DIR\}}/testfile.json") \\
\cmCmd{execute\_process}(\cmConst{COMMAND} TsetSE\_run.py --list --testfile \${testfile}\\
\hspace*{2.7cm}             \cmConst{OUTPUT\_VARIABLE} TsetSE\_tests \cmConst{OUTPUT\_STRIP\_TRAILING\_WHITESPACE} )
\\[1ex]
\cmCmd{foreach}( test  \${TsetSE\_tests})\\
\hspace*{1cm}    \cmCmd{add\_test}(\cmConst{NAME} \${test} \cmConst{COMMAND} TsetSE\_run.py --testfile \${testfile} \${test})\\
\hspace*{1cm}    \cmCmd{message}("Added test \${test}")\\
\cmCmd{endforeach}(test)\\
}\\[1ex]
With this {\tt CMakeLists.txt}, the commands '{\tt make~test}' and '{\tt ctest}' will run all the tests, producing the output
\begin{lstlisting}[basicstyle=\ttfamily,columns=fullflexible]  
Test project /home/bas/programmeren/TsetSE/tests/build
    Start 1: poly3
1/3 Test #1: poly3 ............................   Passed    0.91 sec
    Start 2: intentional_failure
2/3 Test #2: intentional_failure ..............   Passed    0.85 sec
    Start 3: orangina
3/3 Test #3: orangina .........................   Passed    0.89 sec
\end{lstlisting}

The command {\tt ctest -R <testnamepattern>}
will run a selection of tests. The option {\tt -VV} will cause  {\tt ctest} to print all the tests' output. 

\subsection{Using TsetSE with Pytest}
To use TsetSE tests from Pytest, a small script {\tt test\_TsetSE} is needed:
\begin{lstlisting}[language=python,basicstyle=\ttfamily,columns=fullflexible]
from TsetSE.GraphicsComparison import GraphicsComparison, PLOT_NEVER, pytest_init

pytest_init(__file__)

def test_orangina():
    import orangina
    set_graphics(PLOT_NEVER)
    orangina.run()

def test_poly3():
    import poly3
    set_graphics(PLOT_NEVER)
    poly3.run()

def test_intentional_failure():
    import intentional_failure
    set_graphics(PLOT_NEVER)
    intentional_failure.run()
\end{lstlisting}
Running {\tt py.test-3 -v} produces the result
\begin{lstlisting}[basicstyle=\ttfamily,columns=fullflexible]
================================= test session starts ===================================
platform linux -- Python 3.8.5, pytest-4.6.9, py-1.10.0, pluggy-0.13.1 -- /usr/bin/python3
cachedir: .pytest_cache
rootdir: /home/bas/programmeren/TsetSE
collected 3 items

tests/test_TsetSE.py::test_orangina PASSED                                     [ 33%]
tests/test_TsetSE.py::test_poly3 PASSED                                        [ 66%]
tests/test_TsetSE.py::test_intentional_failure PASSED                          [100%]
============================= 3 passed in 0.52 seconds ===================================
\end{lstlisting}

\subsection{Using TsetSE with Unittest}
To use TsetSE tests from {\tt unittest}, a small script {\tt unittest\_TsetSE.py} is needed:
\begin{lstlisting}[language=python,basicstyle=\ttfamily,columns=fullflexible]
#!/usr/bin/env python3
import unittest
from TsetSE.GraphicsComparison import GraphicsComparison, PLOT_NEVER, pytest_init

class TestTsetSE(unittest.TestCase):

    def test_orangina(self):
        import orangina
        set_graphics(PLOT_NEVER)
        orangina.run()
    
    def test_poly3(self):
        import poly3
        set_graphics(PLOT_NEVER)
        poly3.run()
    
    def test_intentional_failure(self):
        import intentional_failure
        set_graphics(PLOT_NEVER)
        intentional_failure.run()

pytest_init(__file__)

if __name__ == '__main__':
    unittest.main()
\end{lstlisting}

Activating this script by typing {\tt ./tests/unittest\_TsetSE.py}, the typical 
{\tt unittest}-output is obtained. This output is not so informative as the 
output from {\tt pytest}, so it is not printed here.

\section{Todo's}
\begin{itemize}
\item Tolerances specified per variable, in a dict;
\item Viewing old results with new plot function, new results with old plot function;
\item Viewing differences with diff-function;
\item User-supplied comparison function?
\end{itemize}

\end{document}
