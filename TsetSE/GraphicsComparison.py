#!/usr/bin/env python3
"""
This module provides the class GraphicsComparison, some constants:
    PLOT_NEVER, PLOT_ALWAYS, PLOT_ON_ERROR
and some functions:
    open axes     open 1 or 2 subplots, and set them to 3d or 2d plotting, depending
                  on what the test needs
    run_plotfun   run the plot function to create plotdata's graphics.
"""
import os
import sys
import inspect
import re
from os.path import isfile
from enum import Enum

import dill as pickle

from matplotlib.widgets import Button

from TsetSE.util import check_ref, myraise

PlotEnum = Enum('plotting options', 'PLOT_ALWAYS PLOT_NEVER PLOT_ON_ERROR ACCEPT_BLINDLY')
PLOT_ALWAYS = PlotEnum.PLOT_ALWAYS
PLOT_NEVER = PlotEnum.PLOT_NEVER
PLOT_ON_ERROR = PlotEnum.PLOT_ON_ERROR
ACCEPT_BLINDLY = PlotEnum.ACCEPT_BLINDLY

graphics = PLOT_NEVER  
max_linelen_docstring = 30

def get_graphics():
    return graphics

def set_graphics(g):
    global graphics
    graphics=g

def set_docstring_linelen(n):
    global max_linelen_docstring
    max_linelen_docstring=n

def pytest_init(calling_file):
    """
    Set paths for TsetSE
    """
    here_dir = os.path.dirname(calling_file)
    result_dir = os.path.join(here_dir,'TsetSEdat') 
    reference_dir = os.path.join(result_dir,'references')
    sys.path.insert(0,here_dir)
    GraphicsComparison.result_dir = result_dir
    if not os.path.isdir(result_dir):
        os.mkdir(result_dir)
    GraphicsComparison.reference_dir = reference_dir
    if not os.path.isdir(reference_dir):
        os.mkdir(reference_dir)

def sample_def():
    """
    This function is just to have something whose type is that of a function,
    so we can test
    if type(x) == type(sample_def)
    """
    return

def open_axes(plotdata, refdata=None, print_docstring=True):
    """
    Open one or two subplots, and set them to 2d or 3d plotting.
    """
    import matplotlib.pyplot as plt
    if plotdata['plot_dimension']==3:
        from mpl_toolkits.mplot3d import Axes3D

    n_extra = 1 if (max_linelen_docstring>0 and print_docstring) else 0
    if refdata is None:
        if plotdata['plot_dimension']==3:
            ax = [plt.gcf().add_subplot(1,1+n_extra,1, projection='3d')]
        else:
            ax = [plt.gcf().add_subplot(1,1+n_extra,1)]
        if max_linelen_docstring>0 and print_docstring:
            ax.append(plt.gcf().add_subplot(1,1+n_extra,1+n_extra))
    else:
        if plotdata['plot_dimension']==3:
            ax = [plt.gcf().add_subplot(1,2+n_extra,1, projection='3d'),
                  plt.gcf().add_subplot(1,2+n_extra,2, projection='3d')]
        else:
            ax = [plt.gcf().add_subplot(1,2+n_extra,1),
                  plt.gcf().add_subplot(1,2+n_extra,2)]
        if max_linelen_docstring>0 and print_docstring:
            ax.append(plt.gcf().add_subplot(1,2+n_extra,2+n_extra))

    # reserve some space at the bottom for buttons
    plt.subplots_adjust(bottom=0.2)
    return ax


def display_doc(text):
    sections = []
    text_so_far = ''
    for line in text.splitlines():
        if re.search('^ *====*$',line):
            prev_text = '\n'.join(text_so_far.splitlines()[:-2])
            new_title = text_so_far.splitlines()[:-1] 
            if prev_text != '' and len(sections) > 0:
                sections[-1]['text'] = prev_text
            if prev_text != '' and len(sections) == 0:
                sections = [{'text': prev_text}]
            sections.append({'title': text_so_far.splitlines()[-1]})
            text_so_far = ''
        elif re.search('^ *$',line):
            text_so_far += '\n'
        else:
            text_so_far = ' '.join([text_so_far,line.strip()])

    prev_text = '\n'.join(text_so_far.splitlines())
    if prev_text != '' and len(sections) > 0:
        sections[-1]['text'] = prev_text
    if prev_text != '' and len(sections) == 0:
        sections = [{'text': prev_text}]

    all_text = ''

    for section in sections:
        all_text += '\n\n'
        if 'title' in section:
            for word in section['title'].split(' '):
                 all_text += '${\\bf ' + word + '}$ '

        if 'text' in section:
           for iline,line in enumerate(section['text'].splitlines()):
               new_line = ''
               new_paragraph = ''
               for word in line.split(' '):
                   if len(new_line)>0 and len(word) + len(new_line) > max_linelen_docstring:
                       new_paragraph = '\n'.join([new_paragraph,new_line])
                       new_line = word
                   elif new_line == '':
                       new_line = word
                   else:
                       new_line += ' '+word
               if new_line != '':
                   new_paragraph = '\n'.join([new_paragraph,new_line])
               all_text = '\n\n'.join([all_text,new_paragraph])
        all_text += '\n\n'

    import matplotlib.pyplot as plt
    plt.text(0,0,all_text)
    plt.axis('off')


def run_plotfun(plotdata, do_plotfun=True ):
    """
    Call the function described by plotdata['plotfun']
    Optionally (only) print the doc-string
    """

    if not isinstance(plotdata['plotfun'],str):
        myraise('plot function should be a string')

    if not re.search(r'^def [a-zA-Z_0-9]* *\(plotdata\):\n',plotdata['plotfun']):
        print(plotdata['plotfun'])
        myraise('plot function does not define a function on plotdata')

    lines = re.sub(r'^def [a-zA-Z_0-9]* *\(plotdata\):\n',
                    'def plot_function(plotdata):\n', plotdata['plotfun'])
    ns = {}
    exec(lines, globals(), ns)
    if do_plotfun:
        ns['plot_function'](plotdata)
    return ns['plot_function'].__doc__

class GraphicsComparison():
    """
    Definition of the class GraphicsComparison, which can graphically compare
    a dict called plotdata to a reference-value of the same dict.
    """

    reference_dir = None
    result_dir = None

    def __init__(self, fname, plotdata, d=3, tol=1e-8):
        """
        Constructor of the class GraphicsComparison.
                fname - name of the output file (without .pickle extension)
                        also used to find the reference file
                plotdata - the dict with the outputs, including the function
                           that creates the plot
                d     - plotting dimension (2 for 2d and 3 for 3d plots)
                tol   - relative tolerance in comparisons
        """

        # Dimension of the plots (2d or 3d)
        self._dim = d
        plotdata['plot_dimension'] = d

        # Figure for this comparison
        self._fig = None

        # Axes for results, references
        self._ax  = [None, None]

        # File name of the result-files
        self._fname = fname

        # Current results [0] and references [1] read succesfully True/False
        self._plotref = None

        # 'Accept' button in figure
        self._baccept = None

        # 'Quit testbank' button in figure
        self._berror = None

        self._inaxes = None


        if graphics != ACCEPT_BLINDLY:
            reffile = os.path.join(GraphicsComparison.reference_dir, fname+".pickle")
            if isfile(reffile):
                print("Reference file '"+reffile+"' exists: read it")
                try:
                    with open(reffile, 'rb') as file:
                        self._plotref = pickle.load(file)
                except Exception as e:
                    print("reference file cannot be read:",e)
            else:
                print("Reference file '%s' does not exist" % reffile)

        if type(plotdata['plotfun']) == type(sample_def):
            lines = inspect.getsource(plotdata['plotfun'])
            plotdata['plotfun'] = lines
        elif not isinstance(plotdata['plotfun'],str):
            myraise('plotfun is not a function and also no string')

        if self._plotref is not None and \
           type(self._plotref['plotfun']) == type(sample_def):
            print("Compatibility: reference's plot function is a function pointer")
            lines = inspect.getsource(self._plotref['plotfun'])
            self._plotref['plotfun'] = lines
            self._plotref['plot_dimension'] = d

        try:
            outfile = os.path.join(GraphicsComparison.result_dir,fname+".pickle")
            with open(outfile, 'wb') as file:
                pickle.dump(plotdata, file)
        except Exception as e:
            print("plotdata is corrupt:",e)
            plotdata = None
            raise RuntimeError('plotdata is corrupt')

        if graphics == ACCEPT_BLINDLY:
            self.accept_as_reference()
            return

        if graphics == PLOT_NEVER:
            self.compare_to_ref(plotdata, tol)
            return

        comparison_error = False
        try:
            self.compare_to_ref(plotdata, tol)
        except Exception as e:
            comparison_error = True
            print(e)

        if graphics == PLOT_ON_ERROR and not comparison_error:
            return

        from matplotlib import cm
        import matplotlib.pyplot as plt
        self._fig = plt.figure()

        if self._plotref is not None:
            print("Reference file successfully read: opening 2 subplots")
            self._ax = open_axes(plotdata, self._plotref)
            self.sca(1)
            try:
                doc_string = run_plotfun(self._plotref)
                plt.title('Reference')
            except Exception:
                print("reference data are corrupt")
                self._plotref = None

        if self._plotref is None:
            from mpl_toolkits.mplot3d import Axes3D
            print("Reference data cannot be displayed. Choosing one subplot")
            plt.close()
            self._fig = plt.figure()
            self._ax = open_axes(plotdata, self._plotref)

        self.sca(0)
        try:
            doc_string = run_plotfun(plotdata)
            plt.title('Current results')
        except Exception as e:
            print("plotdata is corrupt:",e)
            raise RuntimeError('plotdata is corrupt')
        
        if max_linelen_docstring>0:
            plt.sca(self._ax[-1])
            display_doc(doc_string)
            plt.axis('off')
            plt.sca(self._inaxes)

        axaccept = plt.axes([0.05, 0.05, 0.25, 0.05])
        if comparison_error:
            self._baccept = Button(axaccept, 'Accept as reference')
            self._baccept.on_clicked(self.accept_as_reference)
        else:
            self._bclose_figure= Button(axaccept, 'Close Figure')
            self._bclose_figure.on_clicked(self.close_figure)

        if plotdata is not None and self._plotref is not None:
            print("Both plots available")
            self._fig.canvas.mpl_connect('motion_notify_event', self.on_move)
            self._fig.canvas.mpl_connect('resize_event', self.on_resize)

            axerror = plt.axes([0.70, 0.05, 0.25, 0.05])
            self._berror = Button(axerror, 'Quit testbank')
            self._berror.on_clicked(self.error_quit)

        plt.show()
        self.compare_to_ref(plotdata, tol)

    def on_resize(self,event):
        """
        The function that is to be called when one of the axes is rescaled: make the two
        subplots the same again
        """
        self.make_same()

    def make_same(self):
        """
        Set the limits of the 'other' subplot equal to those of the axes that were just now
        reshaped.
        """
        for i,j in enumerate([1,0]):
            if self._inaxes == self._ax[i]:
                if self._dim == 3:
                    self._ax[j].view_init(elev=self._ax[i].elev, azim=self._ax[i].azim)
                    self._ax[j].set_xlim3d(self._ax[i].get_xlim3d())
                    self._ax[j].set_ylim3d(self._ax[i].get_ylim3d())
                    self._ax[j].set_zlim3d(self._ax[i].get_zlim3d())
                else:
                    self._ax[j].set_xlim(self._ax[i].get_xlim())
                    self._ax[j].set_ylim(self._ax[i].get_ylim())
                found = True
        self._fig.canvas.draw_idle()

    def on_move(self,event):
        """
        The function that is to be called when one of the axes is moved: make the two
        subplots the same again
        """
        self._inaxes = event.inaxes
        self.make_same()

    def error_quit(self, event):
        """
        Quit the program when the 'Quit testbank' button is hit.
        """
        import matplotlib.pyplot as plt
        plt.close(self._fig)
        quit()

    def accept_as_reference(self, event=None):
        """
        Function to be run when the 'Accept' button is hit:
        copy output file to reference file, and close the figure.
        The comparison is finalized when this happens.
        """
        import matplotlib.pyplot as plt


        result_file = os.path.join(GraphicsComparison.result_dir, self._fname+".pickle")
        with open(result_file, 'rb') as file:
            self._plotref = pickle.load(file)

        reference_file = os.path.join(GraphicsComparison.reference_dir, self._fname+".pickle")
        with open(reference_file, 'wb') as file:
            pickle.dump(self._plotref, file)
        plt.close(self._fig)

    def close_figure(self, event):
        """
        Function to be run when the 'close figure' button is hit: close the figure
        The comparison is finalized when this happens.
        """
        import matplotlib.pyplot as plt
        print("Close figure")
        plt.close(self._fig)

    def sca(self,i):
        """
        Choose the current output's (i=1) or the reference output's axes to be the current axes
        """
        if self._fig is None:
            return

        import matplotlib.pyplot as plt
        plt.sca(self._ax[i])
        self._inaxes = self._ax[i]

    def compare_to_ref(self, plotdata, tol):
        """
        Compare the contents of the plotdata to the reference data.
        """
        if plotdata is None:
            myraise('current plot data corrupt')

        if self._plotref is None:
            myraise('reference plot data corrupt or unavailable')

        check_ref(plotdata, {"plotdata": self._plotref}, 'plotdata', tol=tol)
