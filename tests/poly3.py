from TsetSE.GraphicsComparison import GraphicsComparison
import numpy as np

def plotfun(plotdata):
    """
    Plot function of test poly3
    ===========================
    This file simply plots a function y(x).
    It shows a 3rd order polynomial.
    """
    import matplotlib.pyplot as plt
    x = plotdata['x']
    y = plotdata['y']
    plt.plot(x,y)
    plt.suptitle('a polynomial of order 3')
    plt.xlabel('x axis')
    plt.ylabel('y axis')

def run():
    x = np.arange(0,2,0.02)
    y = x * (x-0.5) * (x+0.3) + 0.2
    plotdata = { 'x': x, 'y': y, 'plotfun': plotfun}
    GraphicsComparison('poly3',plotdata, d=2)

