#!/usr/bin/env python3
"""
Compare TsetSE data files to the corresponding references
"""
import argparse
import re
import os
import sys
import dill as pickle

import TsetSE.command_line_options 
from TsetSE.GraphicsComparison import GraphicsComparison, \
     set_graphics, set_docstring_linelen, \
     PLOT_NEVER, PLOT_ALWAYS, PLOT_ON_ERROR, run_plotfun

def usage():
    """
    Print the usage of the script
    """
    parser = TsetSE.command_line_options.Parser(main.__doc__)
    parser.add_bool("doc", help="open the user manual (in pdf-viewer), then quit", default=False)
    parser.add_argument("--linelen-doc", type=int, help="max. line-length when printing doc-strings. <n>=0 means: don't print",
                    default=30)
    parser.add_bool("graphics", help="create a plot regardless of the outcome", default=False)
    parser.add_bool("plot-on-error", help="create plots only when test fails", default=False)
    parser.add_argument('files', nargs=argparse.REMAINDER, help = "(pickle) files to compare")

    args = parser.parse_args()
    if args.doc:
        TsetSE.command_line_options.show_doc()
    return args


def main():
    """
    Compare available test results to references
    """
    args = usage()
    graphics = ( PLOT_ALWAYS if args.graphics else
                 PLOT_ON_ERROR if args.plot_on_error else
                 PLOT_NEVER)

    set_docstring_linelen(args.linelen_doc)


    for name in args:
        print("opening ",name)
        try:

            if '.pickle' not in name:
                name += '.pickle'
            with open(name, 'rb') as file:
                plotdata = pickle.load(file)

            GraphicsComparison.result_dir = os.path.dirname(name)
            GraphicsComparison.reference_dir = os.path.join(os.path.dirname(name), 'references')
            fname = re.sub(r'\.pickle','',os.path.basename(name))
            run_plotfun(plotdata, do_plotfun=False)
            set_graphics(graphics)
            GraphicsComparison(fname, plotdata, d=plotdata['plot_dimension'])

        except Exception as e:
            raise(e)

if __name__ == "__main__":
    main()

