{
  description = "TsetSE";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let

        pkgs = import nixpkgs { inherit system; };

        nativeBuildInputs = [ 
          pkgs.texlive.combined.scheme-small 
        ];

        # TODO: onderstaande info staat ook in setup.py. Deze code-duplication kan weg als
        #       je pyproject.toml gaat gebruiken in plaats van setup.py.
        propagatedBuildInputs = builtins.attrValues {
          inherit (pkgs.python3Packages)
            dill
            matplotlib
            numpy
            pyparsing
            python-dateutil
          ;
        };

        checkInputs = [ pkgs.python3Packages.pytestCheckHook ];

        TsetSE =
          pkgs.python3Packages.buildPythonPackage rec {
            pname = "TsetSE";
            version = "0.0.1";
            src = builtins.path { path = ./.; name = pname; };

            inherit nativeBuildInputs propagatedBuildInputs checkInputs;
            pytestFlagsArray = [ "--verbose" ];

            preBuild = ''
              mkdir -p .cache/{texmf-var,mplconfig}
              export XDG_CACHE_HOME=.cache
              export TEXMFHOME=.cache
              export TEXMFVAR=.cache/texmf-var
              export MPLCONFIGDIR=.cache/mplconfig
              xelatex TsetSE_manual.tex
              xelatex TsetSE_manual.tex
              mkdir -p TsetSE/doc
              mv *.pdf TsetSE/doc
            '';

            pythonImportsCheck = [ "TsetSE" ];
          };

      in {

        packages = {
          inherit TsetSE;
          default = TsetSE;
        };
        devShells = {
          default = pkgs.mkShell {
            name = "TsetSE-dev";
            venvDir = "./.venv";
            packages = [
              pkgs.python3Packages.venvShellHook
            ] ++ propagatedBuildInputs ++ [pkgs.python3Packages.pytest] ++ nativeBuildInputs;
          };
        };

      });
}
