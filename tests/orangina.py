from TsetSE.GraphicsComparison import GraphicsComparison
import numpy as np

def plotfun(plotdata):
    """
    Plot function of test orangina
    ==============================
    This file plots a 3d curve (x,y,z)
    The title is 'a swirl, inspired by the Orangina logo' 
    """
    import matplotlib.pyplot as plt
    ax = plt.gca()
    x = plotdata['x']
    y = plotdata['y']
    z = plotdata['z']
    ax.plot(x,y,z, color='tab:orange')
    plt.suptitle('a swirl, inspired by the Orangina logo')
    plt.xlabel('x axis')
    plt.ylabel('y axis')

def run():
    z = np.arange(-1,1,0.02)
    r = np.sqrt(1-z*z)
    theta = z * 12
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    plotdata = { 'x': x, 'y': y, 'z':z, 'plotfun': plotfun}
    GraphicsComparison('orangina',plotdata)

