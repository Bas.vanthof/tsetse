import os
import sys
import argparse

def show_doc():
    os.system("evince " + os.path.join(os.path.dirname(__file__),'doc','TsetSE_manual.pdf') + '&' )
    quit()

class Formatter(
        argparse.ArgumentDefaultsHelpFormatter,
        argparse.RawTextHelpFormatter):
    """
    This class combines some available features from argparse classes
    """


class Parser(argparse.ArgumentParser):
    """
    Configure the command line options.
    """
    def __init__(self, description):
        argparse.ArgumentParser.__init__(self, description=description, formatter_class=Formatter)

    def add_bool(self, name, default=True, help=None):
        self.add_argument("--"+name, action=argparse.BooleanOptionalAction, help=help, default=default)

