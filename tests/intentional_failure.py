import os
import dill as pickle
from TsetSE.GraphicsComparison import GraphicsComparison, PLOT_NEVER, ACCEPT_BLINDLY, graphics, set_graphics
import numpy as np
from poly3 import plotfun
from TsetSE.util import myraise

def plotfun(plotdata):
    """
    'intentional-failure'
    ===========================================
    This file simply plots a function y(x).
    It shows a sin-function, but the reference file
    is a copy of the output from test poly3.
    Therefore, the comparison of the output to the
    reference will fail.
    """
    import matplotlib.pyplot as plt
    import numpy as np
    x = plotdata['x']
    y = plotdata['y']
    plt.plot(x,y)
    plt.text(x[0]+0.1*(x[-1]-x[0]), np.mean(y), "This test is intended to fail.\nDon't hit the Accept-button!" , color='red')
    plt.suptitle('Intentional difference between output and reference')
    plt.xlabel('x axis')
    plt.ylabel('y axis')


def run():
    if graphics == ACCEPT_BLINDLY:
        print("\n\n" +
              "  **************************************************\n" + 
              "  *   For this test, we use PLOT_NEVER even when   *\n" +
              "  *           ACCEPT_BLINDLY was selected          *\n" + 
              "  **************************************************\n\n")
        set_graphics(PLOT_NEVER)
    x = np.arange(0,2,0.02)
    y = np.sin(4*x)
    plotdata = { 'x': np.arange(0,2,0.02), 
                 'y': y, 'plotfun': plotfun}

    TsetSErefdir = os.path.join(os.path.dirname(__file__),'TsetSEdat','references') 
    fname = os.path.join(TsetSErefdir,'poly3.pickle')
    backup = pickle.load(open(fname,'rb'))
    fname = os.path.join(TsetSErefdir,'intentional_failure.pickle')
    pickle.dump(backup, open(fname,'wb'))

    try:
        GraphicsComparison('intentional_failure',plotdata, d=2)
        did_it_fail = False
    except Exception as e:
        print(e)
        print("This comparison failed, as it should")
        did_it_fail = True

    if not did_it_fail:
        myraise("\n\n" +
                "   ******************************************\n"+
                "   *  this comparison should have failed.   *\n"+
                "   *   I suspect you hit the Accept button  *\n"+
                "   ******************************************\n"+
                "\n")

