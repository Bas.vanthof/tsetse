"""
This module provides check_ref, a function that compares data to the reference
data
"""
import json
import numpy as np

def myraise(text):
    """
    Raise a RuntimeError with the given string
    """
    raise RuntimeError(text)

def myvalue(text):
    """
    Raise a ValueError with the given string
    """
    print('\n    '+text+'\n')
    raise ValueError(text)

def rms(array):
    """
    root-mean-square
    """
    return np.sqrt(array.dot(array)/array.size)

def check_reals(name, aval, bval, tol):
    """
    Check whether variables aval and bval are the same within the given relative
    tolerance in 2-norm
    """
    aval = np.array(aval)
    bval = np.array(bval)

    try:
        if np.any( np.isnan(aval) !=  np.isnan(bval) ):
            myvalue("NaN conflict in "+name)
        if np.all( np.isnan(aval) ):
            return 0
    except Exception:
        myvalue("Unable to test isnan")

    aval = aval[np.logical_not(np.isnan(aval)) ]
    bval = bval[np.logical_not(np.isnan(bval)) ]

    if aval.size==0 and bval.size==0:
        return None

    retval = rms(aval-bval) / (1 if np.all(bval==0) else rms(bval))
    if retval > tol:
        myvalue( ("difference %e too large in "+name) % retval)

    return float(retval)

def check_ref(data, ref_in, name='', tol=1e-8, prefix='', verbose=0):
    """
    Check whether data and ref_in are the same according to the given relative tolerance.
    """
    ref = ref_in if name == '' else ref_in[name]

    if type(data) in [np.int64, np.int32]:
        data = float(data)
    if type(ref) in [np.int64, np.int32]:
        ref = float(ref)
    if type(data) != type(ref):
        if not isinstance(ref,tuple):
            myvalue( prefix + name + ' is a %s, reference is %s'
                      % (type(data).__name__, type(ref).__name__))

    if isinstance(data,tuple):
        data = list(data)
        ref = list(ref)

    if isinstance(data,dict):
        if len(ref.keys()) != len(data.keys()):
            myvalue(prefix+name+' is a dict with %d keys, but reference has %d keys'
                     % (len(data.keys()), len(ref.keys())))
        retval = {}
        for key in data:
            if key == "plotfun":
                continue
            if key not in ref:
                myvalue(prefix+name+' is a dict with key %s, but reference does not have it' % key)
            retval[key] = check_ref( data[key], ref[key], '', tol=tol,
                                     prefix=prefix+('' if prefix=='' else '->') + name +
                                                   ('' if name==''   else '->') + key,
                                     verbose=0)
        retval = json.dumps(retval, indent=3)

    elif isinstance(data,list):

        if not isinstance(ref,list):
            myvalue(prefix+name+' is a list, reference is not')
        if len(data) != len(ref):
            myvalue(prefix + name + " is a list of %d items, but reference has %d\n"
                      % (len(data), len(ref)))

        retval = 0
        for i,datai in enumerate(data):
            retvl0 = check_ref( datai, ref[i], '', tol,
                           prefix+('' if name == '' else '->'+name)+'[%d]'%i, verbose=0)

            if i==0:
                retval = retvl0
            elif isinstance(retvl0,dict):
                for key in retvl0:
                    retval[key] = retvl0[key] if retvl0[key]>retval[key] else retval[key]
            elif retval is None and retvl0 is None:
                pass
            elif retval ==0  and retvl0 is None:
                pass
            else:
                retval = retvl0 if retvl0>retval else retval

    elif isinstance(data,str):

        if data!=ref:
            myraise("difference in string "+prefix+name)
        retval = None

    elif isinstance(data,(float,int)) or \
         type(data) in [type(np.zeros(3)),np.complex128,np.int64]:
        retval = check_reals(prefix+name, np.array(data, dtype=np.double),
                             np.array(ref,dtype=np.double), tol)

    else:
        print("type(data)=",type(data))
        print("data=",data)
        print("ref=",ref)
        myraise("How should I compare this variable?")

    if verbose==1:
        print(prefix+name,": ",retval)
    return retval
