#!/usr/bin/env python3
"""
Run the selected tests
"""
import argparse
import traceback
import os
import sys
import time
import json
import importlib
import TsetSE.command_line_options 
from TsetSE.GraphicsComparison import GraphicsComparison, \
     set_graphics, set_docstring_linelen, \
     PLOT_NEVER, PLOT_ALWAYS, PLOT_ON_ERROR, ACCEPT_BLINDLY

def a_test(name, graphics=PLOT_NEVER):
    """
    Run one test: load the module and call the 'run' function
    """
    lib = importlib.import_module(name)
    #import pdb
    #pdb.set_trace()
    set_graphics(graphics)
    lib.run()


def usage():
    """
    Print how to call this script
    """

    parser = TsetSE.command_line_options.Parser(run_tests.__doc__)
    parser.add_argument("--testfile", type=str, help='file, containing the names of the tests')
    parser.add_argument("--testdir", type=str, default=None,
                        help='directory containing the tests (default: dir of <testfile>')
    parser.add_argument("--resultdir", type=str, default=None,
                        help='output directory (default: <testdir>/TsetSEdat')

    parser.add_bool("doc", help="open the user manual (in pdf-viewer), then quit", default=False)
    parser.add_bool("list", help="print the names of available tests, then quit", default=False)
    parser.add_argument("--linelen-doc", type=int, help="max. line-length when printing doc-strings. <n>=0 means: don't print",
                    default=30)
    parser.add_bool("graphics", help="create a plot regardless of the outcome", default=False)
    parser.add_bool("plot-on-error", help="create plots only when test fails", default=False)
    parser.add_bool("accept-blindly", help="accept results as the new reference, without viewing results", default=False)

    parser.add_argument('tests', nargs=argparse.REMAINDER, help = "selected tests: no selection means 'all tests'")

    args = parser.parse_args()
    args = parser.parse_args()
    if args.doc:
        TsetSE.command_line_options.show_doc()
    return args

def run_tests():
    """
    Run the specified tests (or print their names if option --list was used).
    """
    args = usage()
    testfile = args.testfile
    if '.json' not in testfile:
        testfile += '.json'
    all_tests = json.load(open(testfile,'r'))
    if args.list:
        print(";".join(all_tests+['']))
        return

    testdir = args.testdir if args.testdir is not None else os.path.dirname(testfile)
    sys.path.insert(0,testdir)
    print("Looking for tests in directory",testdir)

    resultdir = ( args.resultdir if args.resultdir is not None else
                  os.path.join(os.path.dirname(testfile),'TsetSEdat'))
    if not os.path.isdir(resultdir):
        os.makedirs(resultdir)
    GraphicsComparison.result_dir = resultdir
    print("Writing results to ",resultdir)

    referencedir = os.path.join(resultdir,'references')
    if not os.path.isdir(referencedir):
        os.makedirs(referencedir)
    GraphicsComparison.reference_dir = referencedir
    print("Reading/Writing references from/to ",referencedir)


    graphics = ( PLOT_ALWAYS if args.graphics else
                 PLOT_ON_ERROR if args.plot_on_error else
                 ACCEPT_BLINDLY if args.accept_blindly else
                 PLOT_NEVER)

    set_docstring_linelen(args.linelen_doc)


    nerrors = 0
    nsuccesses = 0
    retval = []
    tests = all_tests if len(args.tests)==0 else args.tests
    for name in tests:
        if name in all_tests:
            print("Running test ",name)
            tstart = time.time()
            try:
                a_test(name,graphics)
                retval.append("     "+"%-20s"%name+" finished successfully in "+
                              "%0.3f seconds"%(time.time()-tstart))
                nsuccesses += 1
            except Exception:
                nerrors += 1
                exc_info = sys.exc_info()
                traceback.print_exception(*exc_info)
                retval.append("     "+"%-20s"%name+" failed                in "
                              +"%0.3f seconds"%(time.time()-tstart))
        elif name not in ['--testfile', testfile, '--list', '--graphics', '--plot-on-error']:
            retval.append("     %-20s is not listed in %s" % (name, testfile))
            nerrors += 1

    print('\n')
    print('   ============================================================\n')
    for line in retval:
        print(line)
    print('\n    ',nsuccesses,'tests passed successfully, ',nerrors,' tests failed\n')
    print('   ============================================================\n\n')

    if not nerrors==0:
        raise RuntimeError({"message": "Not all tests were successful"})
    if nsuccesses==0:
        raise RuntimeError({"message": "No tests have been selected"})

if __name__ == "__main__":

    if '--testfile' not in sys.argv:
        usage()
    else:
        run_tests()
