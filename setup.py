#!/usr/bin/env python3
import setuptools

with open("Readme.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="TsetSE",
    version="0.0.1",
    author="Bas van 't Hof for Leiden University",
    author_email="vanthofbas@gmail.com",
    description="TsetSE: a Tset suite that Shows Everything",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Bas.vanthof/tsetse",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
      "dill>=0.3.3",
      "matplotlib>=3.2",
      "numpy>=1.19.5",
      "pyparsing>=2.4.7",
      "python-dateutil>=2.8.1",
    ],
    include_package_data=True,
    package_data={
            "": ["doc/*.pdf", "lib/*.so", "include/*.h", "tests/*", "tests/TsetSEdat/reference/*"]}
)
