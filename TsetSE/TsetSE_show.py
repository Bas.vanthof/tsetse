#!/usr/bin/env python3
"""
Show the contents of the chosen TsetSE data files
"""
import argparse
import json
import sys
import matplotlib.pyplot as plt
import dill as pickle

from TsetSE.GraphicsComparison import run_plotfun, open_axes, max_linelen_docstring, display_doc
import TsetSE.command_line_options 


def show_plot(name, print_docstring=True, fmt='screen', xtra_options=None, suffix=[]):
    print("opening ",name)
    try:
        if '.pickle' not in name:
            name += '.pickle'
        with open(name, 'rb') as file:
            plotdata = pickle.load(file)
        plt.clf()
        axes = open_axes(plotdata, print_docstring=print_docstring)
        plt.sca(axes[0])
        if xtra_options is not None:
            for key in xtra_options:
                plotdata[key] = xtra_options[key]
        doc_string = run_plotfun(plotdata)
        if max_linelen_docstring>0 and print_docstring:
            plt.sca(axes[-1])
            display_doc(doc_string)
            plt.axis('off')
        if fmt=="screen":
           plt.show()
        else:
           outname = '-'.join([name.replace('.pickle','')]+suffix)+ '.'+fmt 
           plt.savefig(outname)
    except Exception as e:
        raise(e)


def main():
    """
    Show the contents of the TsetSE files specified in the input
    """
    formats = plt.gcf().canvas.get_supported_filetypes()
    formats['screen'] = 'output to screen'

    parser = TsetSE.command_line_options.Parser(main.__doc__)
    parser.add_bool("doc", help="open the user manual (in pdf-viewer), then quit", default=False)
    parser.add_bool("caption", help="output the subplot with the doc-string, next to the figure")
    parser.add_argument("--format", type=str, default='screen', choices=formats.keys(),
                        help="output format, choose from " + json.dumps(formats,indent=3))
    parser.add_argument("file", type=str, help="(pickle) files to be shown")
    args = parser.parse_args()
    if args.doc:
        TsetSE.command_line_options.show_doc()
    show_plot(args.file, print_docstring=args.caption, fmt=args.format)

if __name__ == "__main__":
    main()
