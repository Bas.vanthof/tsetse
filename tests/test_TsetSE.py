from TsetSE.GraphicsComparison import GraphicsComparison, PLOT_NEVER, pytest_init, set_graphics, ACCEPT_BLINDLY

pytest_init(__file__)

def test_orangina():
    import orangina
    set_graphics(ACCEPT_BLINDLY) 
    orangina.run()
    set_graphics(PLOT_NEVER) 
    orangina.run()

def test_poly3():
    import poly3
    set_graphics(ACCEPT_BLINDLY) 
    poly3.run()
    set_graphics(PLOT_NEVER) 
    poly3.run()

def test_intentional_failure():
    import intentional_failure
    set_graphics(PLOT_NEVER) 
    intentional_failure.run()
