# TsetSE: a tset suite that Shows Everything

# Installation

__TsetSE__ is installed automatically as a _submodule_ of __CapSys__. 
When installing __CapSys__, it is not necessary to also install __TsetSE__.

# Manual
The manual for __TsetSE__ is found in __pdfs/TsetSE_manual.pdf__

